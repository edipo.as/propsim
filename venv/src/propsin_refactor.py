from ambiance import Atmosphere
import math
from collections import namedtuple

EngineOutputs = namedtuple('EngineOutputs', ['compressor_pressure_ratio', 'specific_thrust', 'fuel_air_ratio', 'specific_fuel_consumption', 'thermal_efficiency', 'propulsive_efficiency', 'total_efficiency'])

class AircraftEngines:

    def __init__(self, height):
        self.atm = Atmosphere(height)
        self.temperature_at_altitude = self.atm.temperature[0]
        self.pressure_at_altitude = self.atm.pressure[0]
        self.speed_of_sound_at_altitude = self.atm.speed_of_sound[0]

    def _calculate_intermediate_parameters(self, specific_heat_ratio, specific_heat_constant_pressure, fuel_low_heating_value, burner_exit_temperature, compressor_pressure_ratio, mach_number):
        gas_constant = (specific_heat_ratio - 1) / specific_heat_ratio * specific_heat_constant_pressure
        freestream_velocity = self.speed_of_sound_at_altitude * mach_number

        temperature_ratio_freestream = 1 + (specific_heat_ratio - 1) / 2 * mach_number ** 2
        temperature_ratio_compressor = compressor_pressure_ratio ** ((specific_heat_ratio - 1) / specific_heat_ratio)

        temperature_ratio_burner = burner_exit_temperature / self.temperature_at_altitude
        fuel_air_ratio = specific_heat_constant_pressure * self.temperature_at_altitude / fuel_low_heating_value * (temperature_ratio_burner - temperature_ratio_freestream * temperature_ratio_compressor)

        temperature_ratio_turbine = 1 - temperature_ratio_freestream / temperature_ratio_burner * (temperature_ratio_compressor - 1)
        exit_velocity_ratio_speed_of_sound = (2 / (specific_heat_ratio - 1) * temperature_ratio_burner / (temperature_ratio_freestream * temperature_ratio_compressor) * (temperature_ratio_freestream * temperature_ratio_compressor * temperature_ratio_turbine - 1)) ** (1 / 2)

        return gas_constant, freestream_velocity, temperature_ratio_freestream, temperature_ratio_compressor, temperature_ratio_burner, fuel_air_ratio, temperature_ratio_turbine, exit_velocity_ratio_speed_of_sound

    def _calculate_engine_outputs(self, gas_constant, freestream_velocity, temperature_ratio_freestream, temperature_ratio_compressor, temperature_ratio_burner, fuel_air_ratio, temperature_ratio_turbine, exit_velocity_ratio_speed_of_sound, mach_number, compressor_pressure_ratio):
        specific_thrust = self.speed_of_sound_at_altitude * (exit_velocity_ratio_speed_of_sound - mach_number)
        specific_fuel_consumption = fuel_air_ratio / specific_thrust
        thermal_efficiency = 1 - 1 / (temperature_ratio_freestream * temperature_ratio_compressor)
        propulsive_efficiency = 2 * mach_number / (exit_velocity_ratio_speed_of_sound + mach_number)
        total_efficiency = propulsive_efficiency * thermal_efficiency

        return EngineOutputs(compressor_pressure_ratio=compressor_pressure_ratio, specific_thrust=specific_thrust, fuel_air_ratio=fuel_air_ratio, specific_fuel_consumption=specific_fuel_consumption, thermal_efficiency=thermal_efficiency, propulsive_efficiency=propulsive_efficiency, total_efficiency=total_efficiency)

    def ideal_turbojet(self, mach_number, specific_heat_ratio, specific_heat_constant_pressure, fuel_low_heating_value, burner_exit_temperature, compressor_pressure_ratio, batch_size=1, min_compressor_pressure_ratio=0.001, max_compressor_pressure_ratio=40):
        output = []

        compressor_pressure_ratio_increment = 1

        if batch_size <= 0:
            return output
        elif batch_size == 1:
            max_compressor_pressure_ratio = compressor_pressure_ratio
        else:
            compressor_pressure_ratio = min_compressor_pressure_ratio
            compressor_pressure_ratio_increment = (max_compressor_pressure_ratio - min_compressor_pressure_ratio) / batch_size

        while compressor_pressure_ratio <= max_compressor_pressure_ratio:

            gas_constant, freestream_velocity, temperature_ratio_freestream, temperature_ratio_compressor, temperature_ratio_burner, fuel_air_ratio, temperature_ratio_turbine, exit_velocity_ratio_speed_of_sound = self._calculate_intermediate_parameters(specific_heat_ratio, specific_heat_constant_pressure, fuel_low_heating_value, burner_exit_temperature, compressor_pressure_ratio, mach_number)

            engine_outputs = self._calculate_engine_outputs(gas_constant, freestream_velocity, temperature_ratio_freestream, temperature_ratio_compressor, temperature_ratio_burner, fuel_air_ratio, temperature_ratio_turbine, exit_velocity_ratio_speed_of_sound, mach_number, compressor_pressure_ratio)

            if math.isnan(engine_outputs.specific_thrust) or math.isnan(engine_outputs.specific_fuel_consumption) or math.isnan(engine_outputs.fuel_air_ratio) or math.isnan(engine_outputs.propulsive_efficiency) or math.isnan(engine_outputs.thermal_efficiency) or math.isnan(engine_outputs.total_efficiency):
                compressor_pressure_ratio += compressor_pressure_ratio_increment
                continue

            output.append(engine_outputs)
            compressor_pressure_ratio += compressor_pressure_ratio_increment

        return output
