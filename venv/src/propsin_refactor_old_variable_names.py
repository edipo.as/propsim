from ambiance import Atmosphere
import math
from collections import namedtuple

EngineOutputs = namedtuple('EngineOutputs', ['pi_c', 'F_m0', 'f', 'S', 'eta_T', 'eta_P', 'eta_Total'])


class AircraftEngines:

    def __init__(self, height):
        self.atm = Atmosphere(height)
        self.T0 = self.atm.temperature[0]
        self.P0 = self.atm.pressure[0]
        self.a0 = self.atm.speed_of_sound[0]

    def _calculate_intermediate_parameters(self, gamma, cp, hpr, Tt4, pi_c, M0):
        R = (gamma - 1) / gamma * cp
        V0 = self.a0 * M0

        tau_r = 1 + (gamma - 1) / 2 * M0 ** 2
        tau_c = pi_c ** ((gamma - 1) / gamma)

        tau_lambda = Tt4 / self.T0
        f = cp * self.T0 / hpr * (tau_lambda - tau_r * tau_c)

        tau_t = 1 - tau_r / tau_lambda * (tau_c - 1)
        V9_a0 = (2 / (gamma - 1) * tau_lambda / (tau_r * tau_c) * (tau_r * tau_c * tau_t - 1)) ** (1 / 2)

        return R, V0, tau_r, tau_c, tau_lambda, f, tau_t, V9_a0

    def _calculate_engine_outputs(self, R, V0, tau_r, tau_c, tau_lambda, f, tau_t, V9_a0, M0, pi_c):
        F_m0 = self.a0 * (V9_a0 - M0)
        S = f / F_m0
        eta_T = 1 - 1 / (tau_r * tau_c)
        eta_P = 2 * M0 / (V9_a0 + M0)
        eta_Total = eta_P * eta_T

        return EngineOutputs(pi_c=pi_c, F_m0=F_m0, f=f, S=S, eta_T=eta_T, eta_P=eta_P, eta_Total=eta_Total)

    def ideal_turbojet(self, M0, gamma, cp, hpr, Tt4, pi_c, batch_size=1, min_pi_c=0.001, max_pi_c=40):
        output = []

        pi_c_increment = 1

        if batch_size <= 0:
            return output
        elif batch_size == 1:
            max_pi_c = pi_c
        else:
            pi_c = min_pi_c
            pi_c_increment = (max_pi_c - min_pi_c) / batch_size

        while pi_c <= max_pi_c:

            R, V0, tau_r, tau_c, tau_lambda, f, tau_t, V9_a0 = self._calculate_intermediate_parameters(gamma, cp, hpr,
                                                                                                       Tt4, pi_c, M0)
            engine_outputs = self._calculate_engine_outputs(R, V0, tau_r, tau_c, tau_lambda, f, tau_t, V9_a0, M0, pi_c)

            if any(math.isnan(value) for value in engine_outputs):
                pi_c += pi_c_increment
                continue

            output.append(engine_outputs)
            pi_c += pi_c_increment

        return output
