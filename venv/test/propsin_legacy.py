import math
import unittest
from venv.src.propsin_legacy import AircraftEngines


class TestAircraftEngines(unittest.TestCase):

    def test_ideal_turbojet(self):
        height = 11000
        # Test parameters
        M0 = 0.85
        gamma = 1.4
        cp = 1004.5  # J/kgK
        hpr = 42800  # kJ/kg
        Tt4 = 1600  # K
        pi_c = 20


        # Initialize AircraftEngines class

        engine = AircraftEngines(height)

        # Test with batch_size = 1
        result_single = engine.ideal_turbojet(M0, gamma, cp, hpr, Tt4, pi_c, batch_size=1)
        print(result_single)


        min_pi_c = 10
        max_pi_c = 30
        result_batch = engine.ideal_turbojet(M0, gamma, cp, hpr, Tt4, pi_c, batch_size=4, min_pi_c=min_pi_c,
                                             max_pi_c=max_pi_c)

        print(result_batch)

if __name__ == '__main__':
    unittest.main()
