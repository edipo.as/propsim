import unittest
from venv.src.propsin_refactor import AircraftEngines, EngineOutputs


class TestAircraftEngines(unittest.TestCase):

    def test_ideal_turbojet_single_value(self):
        height = 11000

        M0 = 0.85
        gamma = 1.4
        cp = 1004.5  # J/kgK
        hpr = 42800  # kJ/kg
        Tt4 = 1600  # K
        pi_c = 20

        engine = AircraftEngines(height)
        output = engine.ideal_turbojet(M0, gamma, cp, hpr, Tt4, pi_c)

        print(output)

        expected_output = [
            EngineOutputs(20, 78.74350183391326, 0.01923262407261134, 0.00024412624082575326, 0.6071428571428571,
                          0.8163265306122449, 0.49563492063492067)]

        #Expected outputs are shit
        #I dont know how to make correct asserts. Python type system is also shitty

        #Legacy code returned those values
        # {'pi_c': [20], 'F_m0': [909.5810652239314], 'f': [23.847285425328103], 'S': [0.02621787802877921], 'eta_T': [0.6287543726588306], 'eta_P': [0.35552052215752394], 'eta_Total': [0.22353508287649385]}

        #This test returned those values
        #[EngineOutputs(pi_c=20, F_m0=909.5810652239314, f=23.847285425328103, S=0.02621787802877921, eta_T=0.6287543726588306, eta_P=0.35552052215752394, eta_Total=0.22353508287649385)]

        for actual_value, expected_value in zip(output[0], expected_output[0]):
            self.assertAlmostEqual(actual_value, expected_value, places=5)

    def test_ideal_turbojet_multiple_values(self):
        height = 11000

        M0 = 0.85
        gamma = 1.4
        cp = 1004.5  # J/kgK
        hpr = 42800  # kJ/kg
        Tt4 = 1600  # K
        pi_c = 20

        batch_size = 4

        engine = AircraftEngines(height)
        output = engine.ideal_turbojet(M0, gamma, cp, hpr, Tt4, pi_c, batch_size)

        print(output)

        #Expected outputs are shit
        #I dont know how to make correct asserts. Python type system is also shitty

        #Legacy code returned those values
        #EXPECTED = {'pi_c': [10, 15.0, 20.0, 25.0, 30.0], 'F_m0': [887.5681661568327, 905.4083663101486, 909.5810652239314, 908.0638000136533, 903.7293069054186], 'f': [26.309430627438832, 24.928643806321844, 23.847285425328103, 22.945122085159383, 22.1640874368409], 'S': [0.029642152153066263, 0.027533038940113472, 0.02621787802877921, 0.025268182791577408, 0.024525139626970758], 'eta_T': [0.5474465112074083, 0.5969507392108784, 0.6287543726588306, 0.6516845232610418, 0.6693644411619406], 'eta_P': [0.3611534829900479, 0.3565747520327363, 0.35552052215752394, 0.355903136280535, 0.35700073159518614], 'eta_Total': [0.1977122142733058, 0.2128575618098776, 0.22353508287649385, 0.23193656569409002, 0.2389635951986157]}

        #This test returned those values
        #RESULT   = [EngineOutputs(compressor_pressure_ratio=10.00075, specific_thrust=887.572800475602, fuel_air_ratio=26.309189734507466, specific_fuel_consumption=0.029641725974939522, thermal_efficiency=0.5474562083146333, propulsive_efficiency=0.361152278311779, total_efficiency=0.19771505690875768), EngineOutputs(compressor_pressure_ratio=20.000500000000002, specific_thrust=909.5811275593543, fuel_air_ratio=23.847187539656048, specific_fuel_consumption=0.02621776861580707, thermal_efficiency=0.628757024370695, propulsive_efficiency=0.35552050645508015, total_efficiency=0.22353601574145868), EngineOutputs(compressor_pressure_ratio=30.00025, specific_thrust=903.7290445947624, fuel_air_ratio=22.164050800487008, specific_fuel_consumption=0.02452510620639121, thermal_efficiency=0.6693652283852443, propulsive_efficiency=0.3570007982232562, total_efficiency=0.2389639208364244), EngineOutputs(compressor_pressure_ratio=40.0, specific_thrust=891.110702539426, fuel_air_ratio=20.84589680532108, specific_fuel_consumption=0.0233931617541074, thermal_efficiency=0.695454086128803, propulsive_efficiency=0.36023494953575697, total_efficiency=0.2505268676210453)]


        expected_output = [
            EngineOutputs(20, 78.74350183391326, 0.01923262407261134, 0.00024412624082575326, 0.6071428571428571,
                          0.8163265306122449, 0.49563492063492067),
            EngineOutputs(30, 79.17982550585918, 0.020885270867460644, 0.0002640258427652981, 0.6785714285714286,
                          0.804040404040404, 0.5453351800554017),
            EngineOutputs(40, 79.46957124152075, 0.02230557071794769, 0.00028070819552923027, 0.7357142857142858,
                          0.7931034482758621, 0.5835543766578249)
        ]

        for output_item, expected_item in zip(output, expected_output):
            for actual_value, expected_value in zip(output_item, expected_item):
                self.assertAlmostEqual(actual_value, expected_value, places=5)


if __name__ == '__main__':
    unittest.main()
