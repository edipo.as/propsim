import unittest
from venv.src.propsin_refactor_old_variable_names import AircraftEngines

#THIS RUNS MATHING THE VALUES OF LEGACY FOR SINGLE VALUE
#ASSERT DOES NOT WORK
#FOR MULTIPLE VALUES SOME ERROR OCCUR

class TestAircraftEngines(unittest.TestCase):

    def test_ideal_turbojet_single_value(self):
        height = 11000

        M0 = 0.85
        gamma = 1.4
        cp = 1004.5  # J/kgK
        hpr = 42800  # kJ/kg
        Tt4 = 1600  # K
        pi_c = 20

        engine = AircraftEngines(height)
        output = engine.ideal_turbojet(M0, gamma, cp, hpr, Tt4, pi_c)

        print(output)

        expected_output = [(20, 78.74350183391326, 0.01923262407261134, 0.00024412624082575326, 0.6071428571428571,
                            0.8163265306122449, 0.49563492063492067)]


        #Expected outputs are shit
        #I dont know how to make correct asserts. Python type system is also shitty

        #Legacy code returned those values
        #        {'pi_c': [20], 'F_m0': [909.5810652239314], 'f': [23.847285425328103], 'S': [0.02621787802877921], 'eta_T': [0.6287543726588306], 'eta_P': [0.35552052215752394], 'eta_Total': [0.22353508287649385]}

        #This test returned those values
        #[EngineOutputs(pi_c=20, F_m0=909.5810652239314, f=23.847285425328103, S=0.02621787802877921, eta_T=0.6287543726588306, eta_P=0.35552052215752394, eta_Total=0.22353508287649385)]


        for actual_value, expected_value in zip(output[0], expected_output[0]):
            self.assertAlmostEqual(actual_value, expected_value, places=5)

    def test_ideal_turbojet_multiple_values(self):
        height = 11000

        M0 = 0.85
        gamma = 1.4
        cp = 1004.5  # J/kgK
        hpr = 42800  # kJ/kg
        Tt4 = 1600  # K
        pi_c = 20

        batch_size = 4

        engine = AircraftEngines(height)
        output = engine.ideal_turbojet(M0, gamma, cp, hpr, Tt4, pi_c, batch_size)

        print(output)
        #Expected outputs are shit
        #I dont know how to make correct asserts. Python type system is also shitty

        #Legacy code returned those values
        # LEGACY     print    {'pi_c': [20], 'F_m0': [909.5810652239314], 'f': [23.847285425328103], 'S': [0.02621787802877921], 'eta_T': [0.6287543726588306], 'eta_P': [0.35552052215752394], 'eta_Total': [0.22353508287649385]}

        #This processing failed for bath size 4

        expected_output = [
            (20, 78.74350183391326, 0.01923262407261134, 0.00024412624082575326, 0.6071428571428571, 0.8163265306122449,
             0.49563492063492067),
            (30, 79.17982550585918, 0.020885270867460644, 0.0002640258427652981, 0.6785714285714286, 0.804040404040404,
             0.5453351800554017),
            (40, 79.46957124152075, 0.02230557071794769, 0.00028070819552923027, 0.7357142857142858, 0.7931034482758621,
             0.5835543766578249)
        ]

        for output_item, expected_item in zip(output, expected_output):
            for actual_value, expected_value in zip(output_item, expected_item):
                self.assertAlmostEqual(actual_value, expected_value, places=5)


if __name__ == '__main__':
    unittest.main()
